=== Bible References ===
Contributors: bibleonline
Donate link: bibleonline.ru/donate/money/
Tags: bible, scripture, references, jesus christ, holy bible, holy spirit
Requires at least: 3.0.1
Tested up to: 3.7.1
Stable tag: 1.1.3
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Automatically replace Bible references to a hyperlink with Bible text. Website http://bibleonline.ru/tools/ref/

== Description ==

Description only in Russian.

Данный плагин является инструментом по подсветки ссылок из Библии на вашем сайте.
Установив данный плагин, вы позволите посетителям вашего сайта сразу прочесть, то или иное место из Библии, 
просто наведя на стих курсор мыши.

На пример вы упомянули у себя "Первое послание к Тимофею 3:16", текст данного места появляется, 
как только посетитель наведет курсор мыши на данный стих.  

Более того, ваш читатель может прочитать это место в контексте данной главы, просто нажав на данный стих.

== Installation ==

1. Upload `bible-references` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

F.A.Q. only in Russian.

= Как я узнаю, что скрипт работает? =

Любые ссылки на Библейские стихи на вашей странице, будут заменены на гиперссылки.

= Почему у меня не работает скрипт? =

* Убедитесь, что JavaScript код присутствует на странице и находится непосредственно перед закрывающимся тегом <body>.
* Убедитесь, что ваш браузер поддерживает JavaScript, и JavaScript включен в настройках браузера.
* Убедитесь, что ваша ссылка на Библейский текст не находится в теге (или классе), который вы запретили для использования в поиске.

= На каких языках работает подстветка стихов? =

* Поиск Библейских книг сегодня происходит на Русском и Английском языке. В качестве подсветки книг, можно использовать любой из переводов присутствующих на сайте.

== Screenshots ==

1. Configuration of plugin
2. An example work of the plugin

== Changelog ==
= 1.1.3 =
* Исправлена ошибка с сбросом настроек
* Исправлен скрипт разбора ссылок, решены проблемы с посланиями Иоанна и книгой Деяний

= 1.1.2 = 
* Исправлена ошибка с определением 4 книги царств и ряд других книг
* Устранена ошибка при входе в панель администрирования без прав администратора

= 1.1.1 =
* Default option fix. If you use previous version of plugin, please replace in `Separators verse from verses` field: "." (dot) to "," (commaa).
* Если вы используете предыдущию версию плагина, измените `Разделители стихов от стихов` c "." (точки) на "," (запятую).

= 1.1 = 
* Stable release
* Now you can configure plugin
* Plugin supported multi languages

= 1.0 =
* Plugin in beta testing mode

== License ==

GNU General Public License v2

